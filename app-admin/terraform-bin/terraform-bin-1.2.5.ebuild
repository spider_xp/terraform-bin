# Copyright 1999-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DESCRIPTION="Official Hashicorp Terraform (binary package)"
HOMEPAGE="https://terraform.io"

S="${WORKDIR}"

SRC_URI="
	amd64? ( https://releases.hashicorp.com/terraform/${PV}/terraform_${PV}_linux_amd64.zip )
	arm?   ( https://releases.hashicorp.com/terraform/${PV}/terraform_${PV}_linux_arm.zip )
	arm64? ( https://releases.hashicorp.com/terraform/${PV}/terraform_${PV}_linux_arm64.zip )
	x86?   ( https://releases.hashicorp.com/terraform/${PV}/terraform_${PV}_linux_386.zip )
"

BDEPEND="app-arch/unzip"

LICENSE="MPL-2.0"
SLOT="1.2"
KEYWORDS="-* ~amd64 ~arm ~arm64 ~x86"

src_install() {
	newbin terraform terraform-bin-${SLOT}
}
